﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using CommonLibrary;

namespace Judging_Client
{

	/// <summary>
	/// Interaction logic for ButtonControl.xaml
	/// </summary>
	public partial class ButtonControlV1 : UserControl, System.ComponentModel.INotifyPropertyChanged
	{
	    private List<Button> _buttons = new List<Button>();
	    private Brush _background;

        public static readonly DependencyProperty ValueProperty = DependencyProperty.Register(
                "Value", typeof(double), typeof(ButtonControlV1), new FrameworkPropertyMetadata(0.0, new PropertyChangedCallback(ValuePropertyChanges)){ BindsTwoWayByDefault = true }
            );

        public static readonly DependencyProperty AreaProperty = DependencyProperty.Register(
                "Area", typeof(string), typeof(ButtonControlV1), new FrameworkPropertyMetadata("", new PropertyChangedCallback(AreaPropertyChanges)){ BindsTwoWayByDefault = false }
            );

        public double Value
        {
            get { return (double)GetValue(ValueProperty);  }
            set
            {
                SetValue(ValueProperty, value);
            }
        }

        public string Area
        {
            get { return (string)GetValue(AreaProperty); }
            set { SetValue(AreaProperty, value); }
        }

        private Button getButton(double value)
        {
            string sval = value.ToString("#0.0").Replace('.', ',');
            if (sval.EndsWith("0"))
            {
                sval = sval.Substring(0, sval.Length - 2);
            }
            return _buttons.SingleOrDefault(b => b.Content.ToString() == sval);
        }

        private void RenderControl()
        {
            // Alle Buttons zurücksetzen

            foreach (var obj in _buttons)
            {
                if (obj is Button)
                {
                    var b = (Button)obj;
                    try
                    {
                        b.Background = Brushes.DarkGray;
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine(e.Message);
                    }
                }
            }
            // Abhängig von unserem Wert setzen wir nun den richtigen Wert / Button
            var current = getButton((double)GetValue(ValueProperty));
            
            if (current != null)
            {
                current.Background = Brushes.Green;
            }
        }

		public ButtonControlV1()
		{
			this.InitializeComponent();

		    

            Button10.Click += new RoutedEventHandler(Button_Click);
            Button9.Click += new RoutedEventHandler(Button_Click);
            Button8.Click += new RoutedEventHandler(Button_Click);
            Button7.Click += new RoutedEventHandler(Button_Click);
            Button6.Click += new RoutedEventHandler(Button_Click);
            Button5.Click += new RoutedEventHandler(Button_Click);
            Button4.Click += new RoutedEventHandler(Button_Click);
            Button3.Click += new RoutedEventHandler(Button_Click);
            Button2.Click += new RoutedEventHandler(Button_Click);
            Button1.Click += new RoutedEventHandler(Button_Click);
            
            Button95.Click += new RoutedEventHandler(Button_Click);
            Button85.Click += new RoutedEventHandler(Button_Click);
            Button75.Click += new RoutedEventHandler(Button_Click);
            Button65.Click += new RoutedEventHandler(Button_Click);
            Button55.Click += new RoutedEventHandler(Button_Click);
            Button45.Click += new RoutedEventHandler(Button_Click);
            Button35.Click += new RoutedEventHandler(Button_Click);
            Button25.Click += new RoutedEventHandler(Button_Click);
            Button15.Click += new RoutedEventHandler(Button_Click);
            Button05.Click += new RoutedEventHandler(Button_Click);

            _buttons.Add(Button10);
            _buttons.Add(Button9);
            _buttons.Add(Button8);
            _buttons.Add(Button7);
            _buttons.Add(Button6);
            _buttons.Add(Button5);
            _buttons.Add(Button4);
            _buttons.Add(Button3);
            _buttons.Add(Button2);
            _buttons.Add(Button1);

            _buttons.Add(Button95);
            _buttons.Add(Button85);
            _buttons.Add(Button75);
            _buttons.Add(Button65);
            _buttons.Add(Button55);
            _buttons.Add(Button45);
            _buttons.Add(Button35);
            _buttons.Add(Button25);
            _buttons.Add(Button15);
            _buttons.Add(Button05);

            RenderControl();
		}

        void Button_Click(object sender, RoutedEventArgs e)
        {
            e.Handled = true;
            Button button = (Button) sender;
            string text = (string)button.Content;
            
            double val = text.ParseString();

            if (val == Value)
            {
                // Reset value:
                Background = _background;
                SetValue(ValueProperty, 0.0);
                return;
            }

            SetValue( ValueProperty, val);
            HandlePropertyChanged("Value");
            // Set new Background:
            if(this.Background != Brushes.Green)
                _background = this.Background;
            this.Background = Brushes.Green;
            this.Focus();
            RenderControl();
        }

        public void ResetBackground()
        {
            if (_background != null)
                this.Background = _background;
        }

        private static void ValuePropertyChanges(DependencyObject o, DependencyPropertyChangedEventArgs e)
        {
            if (o is ButtonControlV1)
            {
                ButtonControlV1 bc = (ButtonControlV1)o;
                bc.RenderControl();
            }
        }

        private static void AreaPropertyChanges(DependencyObject o, DependencyPropertyChangedEventArgs e)
        {
            if (o is ButtonControlV1)
            {
                ButtonControlV1 bc = (ButtonControlV1)o;
                bc.AreaLabel.Content = "" + e.NewValue;
                bc.AreaLabel2.Content = "" + e.NewValue;
            }
        }

        private void HandlePropertyChanged(string name)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(name));
        }

        #region INotifyPropertyChanged Members

        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;

        #endregion
    }
}