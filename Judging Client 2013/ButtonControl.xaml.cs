﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;

namespace Judging_Client
{
    public class ChangedDataEventArgs : EventArgs
    {
        public int Area { get; set; }
        public double NewValue { get; set; }
        public double OldValue { get; set; }
    }

    public class ButtonViewModel : ViewModelBase
    {
        private double value;

        private bool isSelected;

        private Brush backgroundBrushSelected;

        private Brush backgroundBrushUnselected;

        public ButtonViewModel()
        {

            backgroundBrushSelected = (Brush)App.Current.FindResource("ButtonSelectedBrush");
            backgroundBrushUnselected = (Brush)App.Current.FindResource("ButtonUnselectedBrush");

        }

        public double Value
        {
            get
            {
                return this.value;
            }
            set
            {
                this.value = value;
                RaisePropertyChanged(() => Value);
            }
        }

        public bool IsSelected
        {
            get
            {
                return this.isSelected;
            }
            set
            {
                this.isSelected = value;
                RaisePropertyChanged(() => IsSelected);
                RaisePropertyChanged(() => BackgroundColorBrush);
            }
        }

        public Brush BackgroundColorBrush
        {
            get
            {
                return isSelected ? backgroundBrushSelected : backgroundBrushUnselected; 
            }
        }
    }

    public delegate void ChangedEventHandler(object sender, ChangedDataEventArgs e);
	/// <summary>
	/// Interaction logic for ButtonControl.xaml
	/// </summary>
	public partial class ButtonControl : UserControl, System.ComponentModel.INotifyPropertyChanged
	{
	    private Brush background;

        public ButtonControl()
        {
            this.InitializeComponent();

            ButtonClickCommand = new RelayCommand<ButtonViewModel>(ButtonClick);

            buttonsViewModel = new List<ButtonViewModel>();
            LeftColumnValues = new ObservableCollection<ButtonViewModel>();
            RightColumnValues = new ObservableCollection<ButtonViewModel>();

            this.MinimumValue = 0.5;
            this.MaximumValue = 10.0;
            this.Intervall = 0.5;

            this.DataContext = this;

            RenderControl();
        }

        public static readonly DependencyProperty ValueProperty = DependencyProperty.Register(
                "Value", typeof(double), typeof(ButtonControl), new FrameworkPropertyMetadata(0.0, ValuePropertyChanges){ BindsTwoWayByDefault = true }
            );

        public static readonly DependencyProperty AreaProperty = DependencyProperty.Register(
                "Area", typeof(string), typeof(ButtonControl), new FrameworkPropertyMetadata("", AreaPropertyChanges){ BindsTwoWayByDefault = false }
            );

        public static readonly DependencyProperty MinimumValueProperty = DependencyProperty.Register(
                "MinimumValue", typeof(double), typeof(ButtonControl), new FrameworkPropertyMetadata(0.5, MinimumValuePropertyChanges) { BindsTwoWayByDefault = false }
            );

        public static readonly DependencyProperty MaximumValueProperty = DependencyProperty.Register(
                "MaximumValue", typeof(double), typeof(ButtonControl), new FrameworkPropertyMetadata(0.5, MaximumValuePropertyChanges) { BindsTwoWayByDefault = false }
            );

        public static readonly DependencyProperty IntervallProperty = DependencyProperty.Register(
           "Intervall", typeof(double), typeof(ButtonControl), new FrameworkPropertyMetadata(0.5, IntervallValuePropertyChanges) { BindsTwoWayByDefault = false }
       );   

        public double Value
        {
            get { return (double)GetValue(ValueProperty);  }
            set
            {
                SetValue(ValueProperty, value);
            }
        }

        public string Area
        {
            get { return (string)GetValue(AreaProperty); }
            set { SetValue(AreaProperty, value); }
        }

	    public double MinimumValue
	    {
            get
            {
                return (double)this.GetValue(MinimumValueProperty); 
            }

	        set
	        {
	            SetValue(MinimumValueProperty, value);
	        }
	    }

	    public double Intervall
	    {
	        get
	        {
	            return (double)this.GetValue(IntervallProperty);
	        }
	        set
	        {
	            SetValue(IntervallProperty, value);
	        }
	    }

	    public double MaximumValue
	    {
	        get
	        {
	            return (double)this.GetValue(MaximumValueProperty);
	        }

	        set
	        {
	            SetValue(MaximumValueProperty, value);
	        }
	    }

        public ObservableCollection<ButtonViewModel> LeftColumnValues { get; set; }

        public ObservableCollection<ButtonViewModel> RightColumnValues { get; set; }

        public ICommand ButtonClickCommand { get; set; }

        private List<ButtonViewModel> buttonsViewModel { get; set; }

	    private void CreateModel()
	    {
            // Re-Create the observalble lists:
            LeftColumnValues.Clear();
            RightColumnValues.Clear();
            buttonsViewModel.Clear();

            var value = MaximumValue;
            while (value >= MinimumValue)
            {
                var viewModel = new ButtonViewModel() { IsSelected = false, Value = value };
                LeftColumnValues.Add(viewModel);
                buttonsViewModel.Add(viewModel);

                value -= this.Intervall;
                if (value < MinimumValue)
                {
                    return;
                }
                viewModel = new ButtonViewModel() { IsSelected = false, Value = value };
                RightColumnValues.Add(viewModel);
                buttonsViewModel.Add(viewModel);

                value -= this.Intervall;
            }
	    }

	    private void RenderControl()
	    {
	        foreach (var buttonViewModel in buttonsViewModel)
	        {
	            buttonViewModel.IsSelected = false;
	        }

	        var element = buttonsViewModel.SingleOrDefault(b => Math.Abs(b.Value - this.Value) < 0.001);
	        if (element != null)
	        {
	            element.IsSelected = true;
	            this.Background = Brushes.Green;
	        }
	    }



        void ButtonClick(ButtonViewModel model)
        {
            if (model.Value != this.Value)
            {
                SetValue(ValueProperty, model.Value);
                HandlePropertyChanged("Value");
                // Set new Background:
                if (this.Background != Brushes.Green)
                {
                    background = this.Background;
                }

                this.Background = Brushes.Green;
            }
            else
            {
                this.Background = Brushes.DarkRed;
                this.Value = 0;
            }

            this.Focus();
            RenderControl();
        }

        public void ResetBackground()
        {
            this.Background = Brushes.DarkRed;
        }

        private static void ValuePropertyChanges(DependencyObject o, DependencyPropertyChangedEventArgs e)
        {
            var buttonControl = o as ButtonControl;
            if (buttonControl != null)
            {
                buttonControl.RenderControl();
            }
        }

        private static void AreaPropertyChanges(DependencyObject o, DependencyPropertyChangedEventArgs e)
        {
            if (o is ButtonControl)
            {
                ButtonControl bc = (ButtonControl)o;
                bc.AreaLabel.Content = "" + e.NewValue;
                bc.AreaLabel2.Content = "" + e.NewValue;
            }
        }

	    private static void MinimumValuePropertyChanges(DependencyObject o, DependencyPropertyChangedEventArgs e)
	    {
	        var buttonControl = o as ButtonControl;
	        if (buttonControl != null)
	        {
                buttonControl.CreateModel();
	            buttonControl.RenderControl();
	        }
	    }

        private static void MaximumValuePropertyChanges(DependencyObject o, DependencyPropertyChangedEventArgs e)
        {
            var buttonControl = o as ButtonControl;
            if (buttonControl != null)
            {
                buttonControl.CreateModel();
                buttonControl.RenderControl();
            }
        }

        private static void IntervallValuePropertyChanges(DependencyObject o, DependencyPropertyChangedEventArgs e)
        {
            var buttonControl = o as ButtonControl;
            if (buttonControl != null)
            {
                buttonControl.CreateModel();
                buttonControl.RenderControl();
            }
        }

        private void HandlePropertyChanged(string name)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(name));                
            }
        }

        #region INotifyPropertyChanged Members

        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;

        #endregion
    }
}