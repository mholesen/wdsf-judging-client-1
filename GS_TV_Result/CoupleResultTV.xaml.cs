﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace TVGraphics
{
    /// <summary>
    /// Interaction logic for CoupleResultTV.xaml
    /// </summary>
    public partial class CoupleResultTV : Page
    {
        public CoupleResultTV(Models.CoupleResultModel model)
        {
            InitializeComponent();

            ShowsNavigationUI = false;

            // Wir zeigen die Infos an:
            this.ScoreA.Content = String.Format("{0:#.00}", model.MarkA);
            this.ScoreB.Content = String.Format("{0:#.00}", model.MarkB);
            this.ScoreC.Content = String.Format("{0:#.00}", model.MarkC);
            this.ScoreD.Content = String.Format("{0:#.00}", model.MarkD);
            this.ScoreE.Content = String.Format("{0:#.00}", model.MarkE);
            this.ScoreSum.Content = String.Format("{0:#.00}", model.Sum);
            this.Place.Content = model.Place;
            this.NameMan.Content = model.NameMan;
            this.NameWoman.Content = model.NameWoman;
            this.Country.Content = model.Country;

            // Test.Content = this.RenderSize.Width + " x " + this.RenderSize.Height;
        }
    }
}
