using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

namespace Judging_Server.Model
{
    public class StateToVisibilityConverter : IValueConverter
    {
        public object Convert(object value, Type targetType,
                              object parameter, CultureInfo culture)
        {
            var s = (States)value;
            switch (s)
            {
                case States.NotUsed: return Visibility.Hidden;
                default: return Visibility.Visible;
            }
        }

        public object ConvertBack(object value, Type targetType,
                                  object parameter, CultureInfo culture)
        {
            return null;
        }
    }
}