﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Judging_Server.ReportModels
{
    public class Heat
    {
        public string Dance { get; set; }
        public int Order { get; set; }

        public int HeatNo { get; set; }

        public string NumbersAsString
        {
            get { return "Not Implemented"; }
        }

        public List<Model.Couple> Participants { get; set; }

        // This is not realy nice but report bindung to a variable list seams not working
        public string P1
        {
            get { return Participants.Count > 0 ? Participants[0].Id.ToString() : ""; }
        }

        public string P2
        {
            get { return Participants.Count > 1 ? Participants[1].Id.ToString() : ""; }
        }

        public string P3
        {
            get { return Participants.Count > 2 ? Participants[2].Id.ToString() : ""; }
        }

        public string P4
        {
            get { return Participants.Count > 3 ? Participants[3].Id.ToString() : ""; }
        }

        public string P5
        {
            get { return Participants.Count > 4 ? Participants[4].Id.ToString() : ""; }
        }

        public string P6
        {
            get { return Participants.Count > 5 ? Participants[5].Id.ToString() : ""; }
        }

        public string P7
        {
            get { return Participants.Count > 6 ? Participants[6].Id.ToString() : ""; }
        }

        public string P8
        {
            get { return Participants.Count > 7 ? Participants[7].Id.ToString() : ""; }
        }

        public string P9
        {
            get { return Participants.Count > 8 ? Participants[8].Id.ToString() : ""; }
        }

        public string P10
        {
            get { return Participants.Count > 9 ? Participants[9].Id.ToString() : ""; }
        }

        public string P11
        {
            get { return Participants.Count > 10 ? Participants[10].Id.ToString() : ""; }
        }

        public string P12
        {
            get { return Participants.Count > 11 ? Participants[11].Id.ToString() : ""; }
        }

        public string P13
        {
            get { return Participants.Count > 12 ? Participants[12].Id.ToString() : ""; }
        }

        public string P14
        {
            get { return Participants.Count > 13 ? Participants[13].Id.ToString() : ""; }
        }

        public string P15
        {
            get { return Participants.Count > 14 ? Participants[14].Id.ToString() : ""; }
        }

        public string P16
        {
            get { return Participants.Count > 15 ? Participants[15].Id.ToString() : ""; }
        }

        public string P17
        {
            get { return Participants.Count > 16 ? Participants[16].Id.ToString() : ""; }
        }

        public string P18
        {
            get { return Participants.Count > 17 ? Participants[17].Id.ToString() : ""; }
        }

        public string P19
        {
            get { return Participants.Count > 18 ? Participants[18].Id.ToString() : ""; }
        }

        public string P20
        {
            get { return Participants.Count > 19 ? Participants[19].Id.ToString() : ""; }
        }
    }
}
