﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Judging_Server.ReportModels
{
    public class CoupleHeats
    {
        public string Dance { get; set; }
        public int Number { get; set; }
        public int HeatDance_A { get; set; }
        public int HeatDance_B { get; set; }
        public int HeatDance_C { get; set; }
        public int HeatDance_D { get; set; }
        public int HeatDance_E { get; set; }
    }
}
