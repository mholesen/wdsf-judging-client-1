﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Judging_Server.Model
{
    public class DanceResultV2 : DanceResultBase
    {
        public DanceResultV2(List<JudgingArea> judgingAreas, Couple couple, string dance, List<Judge> judges) : base(judgingAreas, couple, dance, judges)
        {
            
        }

        /// <summary>
        /// CalculateTotal calculates the points in this dance
        /// </summary>
        protected override void CalculateTotal()
        {
            foreach (TotalPerCouple p in _couplesTotal)
                p.State = (int)States.Used;

            // Todo: It Could be less then 12 in the future
            // a static value might not be the best way
            if (_judgements.Count < 12) return;
            
            // Get the min and max values per Area
            var minA = _judgements.Any(j => j.Value.MarkA > -1) ? _judgements.Where(j => j.Value.MarkA > -1).Min(j => j.Value.MarkA) : 0;
            var minB = _judgements.Any(j => j.Value.MarkB > -1) ? _judgements.Where(j => j.Value.MarkB > -1).Min(j => j.Value.MarkB) : 0;
            var minC = _judgements.Any(j => j.Value.MarkC > -1) ? _judgements.Where(j => j.Value.MarkC > -1).Min(j => j.Value.MarkC) : 0;
            var minD = _judgements.Any(j => j.Value.MarkD > -1) ? _judgements.Where(j => j.Value.MarkD > -1).Min(j => j.Value.MarkD) : 0;
            var minE = _judgements.Any(j => j.Value.MarkE > -1) ? _judgements.Where(j => j.Value.MarkE > -1).Min(j => j.Value.MarkE) : 0;

            var maxA = _judgements.Any(j => j.Value.MarkA > -1) ? _judgements.Where(j => j.Value.MarkA > -1).Max(j => j.Value.MarkA) : 0;
            var maxB = _judgements.Any(j => j.Value.MarkB > -1) ? _judgements.Where(j => j.Value.MarkB > -1).Max(j => j.Value.MarkB) : 0;
            var maxC = _judgements.Any(j => j.Value.MarkC > -1) ? _judgements.Where(j => j.Value.MarkC > -1).Max(j => j.Value.MarkC) : 0;
            var maxD = _judgements.Any(j => j.Value.MarkD > -1) ? _judgements.Where(j => j.Value.MarkD > -1).Max(j => j.Value.MarkD) : 0;
            var maxE = _judgements.Any(j => j.Value.MarkE > -1) ? _judgements.Where(j => j.Value.MarkE > -1).Max(j => j.Value.MarkE) : 0;

            RaiseEvent("Marking_A");

            double sumA = 0;
            double sumB = 0;
            double sumC = 0;
            double sumD = 0;
            double sumE = 0;

            double numJudges = 0;
            foreach (var judgement in _judgements.Values)
            {
                sumA += judgement.MarkA > -1 ? judgement.MarkA : 0;
                sumB += judgement.MarkB > -1 ? judgement.MarkB : 0;
                sumC += judgement.MarkC > -1 ? judgement.MarkC : 0;
                sumD += judgement.MarkD > -1 ? judgement.MarkD : 0;
                sumE += judgement.MarkE > -1 ? judgement.MarkE : 0;
                numJudges++;
            }
            sumA = sumA * 2 - minA - maxA;
            sumB = sumB * 2 - minB - maxB;
            sumC = sumC * 2 - minC - maxC;
            sumD = sumD * 2 - minD - maxD;
            sumE = sumE * 2 - minE - maxE;
            // Mittelwert auf die Anzahl der Judge
            var judgementsA = _judgements.Values.Count(c => c.MarkA > -1);
            var judgementsB = _judgements.Values.Count(c => c.MarkB > -1);
            var judgementsC = _judgements.Values.Count(c => c.MarkC > -1);
            var judgementsD = _judgements.Values.Count(c => c.MarkD > -1);
            var judgementsE = _judgements.Values.Count(c => c.MarkE > -1);

            sumA /= (judgementsA * 2 - 2);
            sumB /= (judgementsB * 2 - 2);
            sumC /= (judgementsC * 2 - 2);
            sumD /= (judgementsD * 2 - 2);
            sumE /= (judgementsE * 2 - 2);
            _result_A = sumA;
            _result_B = sumB;
            _result_C = sumC;
            _result_D = sumD;
            _result_E = sumE;
            // We add chairman reductions (if applied) and
            // calculate the sum based on the weight of each component
            double sum = sumA * (_judgmentAreas.Count > 0 ? _judgmentAreas[0].Weight : 1) +
                         sumB * (_judgmentAreas.Count > 1 ? _judgmentAreas[1].Weight : 1) +
                         sumC * (_judgmentAreas.Count > 2 ? _judgmentAreas[2].Weight : 1) +
                         sumD * (_judgmentAreas.Count > 3 ? _judgmentAreas[3].Weight : 1) +
                         sumE * (_judgmentAreas.Count > 4 ? _judgmentAreas[4].Weight : 1) +
                         ChairmanReduction;

            sum = Math.Round(sum, 3, MidpointRounding.AwayFromZero);
            if (sum != Total)
            {
                Total = sum;
                RaiseEvent("Total");
                RaiseEvent("Result_A");
                RaiseEvent("Result_B");
                RaiseEvent("Result_C");
                RaiseEvent("Result_D");
            }
        }

    }
}
