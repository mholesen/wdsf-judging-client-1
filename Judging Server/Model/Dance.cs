namespace Judging_Server.Model
{
    public class Dance
    {
        public string ShortName { get; set; }
        public string LongName { get; set; }
    }
}