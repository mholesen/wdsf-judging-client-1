﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Judging_Server.Model
{
    public enum CalculationVersionEnum
    {
        Version1 = 1,
        Version2 = 2,
        Version2014 = 3
    }
    /// <summary>
    /// Creates our DanceResult Object that calculates the results based on the version
    /// </summary>
    class DanceResultFactory
    {
        /// <summary>
        /// Creates a new DanceResult object based on the version
        /// </summary>
        /// <param name="version">Version of calculation</param>
        /// <param name="judgingAreas">List of judging Areas (relevant for V2, V2014</param>
        /// <param name="couple">The couple</param>
        /// <param name="dance">The dance</param>
        /// <param name="judges">List of judges</param>
        /// <returns>new created object of desired type</returns>
        public static DanceResultBase GetDanceResult(CalculationVersionEnum version, List<JudgingArea> judgingAreas, Couple couple, string dance, List<Judge> judges)
        {
            switch(version)
            {
                case CalculationVersionEnum.Version1:
                    return new DanceResultV1(judgingAreas, couple, dance, judges);
                case CalculationVersionEnum.Version2:
                    return new DanceResultV2(judgingAreas, couple, dance, judges);
                case CalculationVersionEnum.Version2014:
                    return new DanceResult2014(judgingAreas, couple, dance, judges);
            }

            throw new Exception("Wrong Calculation Version in Factory!");
        }
    }
}
