﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Judging_Server.Model
{
    /// <summary>
    /// Dance Result Implementation for the Judging System 2.1 implementation (distance based weights)
    /// </summary>
    class DanceResult2014 : DanceResultBase
    {

        /// <summary>
        /// Initializes a new instance of the <see cref="DanceResult2014"/> class.
        /// </summary>
        /// <param name="judgingAreas">The judging areas.</param>
        /// <param name="couple">The couple.</param>
        /// <param name="dance">The dance.</param>
        /// <param name="judges">The judges.</param>
        public DanceResult2014(List<JudgingArea> judgingAreas, Couple couple, string dance, List<Judge> judges) : base(judgingAreas, couple, dance, judges)
        {
            
        }

        /// <summary>
        /// Calculates the sum according to the new rules in 2014.
        /// </summary>
        /// <param name="marks">The marks.</param>
        /// <returns></returns>
        private double CalculateSumV3(List<double> marks)
        {
            var distance = marks[1] - marks[0];
            var lowerWeight =  1 / (1 + distance * distance);
            distance = marks[2] - marks[1];
            var upperWeight = 1 / (1 + distance * distance);
            var sum = marks[0] * lowerWeight + marks[1] + marks[2] * upperWeight;
            sum /= 1 + 1 * lowerWeight + 1 * upperWeight;

            return sum;
        }

        /// <summary>
        /// Calculates the total sum for a dance.
        /// </summary>
        protected override void CalculateTotal()
        {
            foreach (TotalPerCouple p in _couplesTotal)
                p.State = (int)States.Used;

            if (_judgements.Count < 12) return;

            var marksA = _judgements.Where(j => j.Value.MarkA > -1).OrderBy(j => j.Value.MarkA).Select(j => j.Value.MarkA).ToList();
            var marksB = _judgements.Where(j => j.Value.MarkB > -1).OrderBy(j => j.Value.MarkB).Select(j => j.Value.MarkB).ToList();
            var marksC = _judgements.Where(j => j.Value.MarkC > -1).OrderBy(j => j.Value.MarkC).Select(j => j.Value.MarkC).ToList();
            var marksD = _judgements.Where(j => j.Value.MarkD > -1).OrderBy(j => j.Value.MarkD).Select(j => j.Value.MarkD).ToList();

            if (marksA.Count != 3 || marksB.Count != 3 || marksC.Count != 3 || marksD.Count != 3) return;

            RaiseEvent("Marking_A");

            double sumA = 0;
            double sumB = 0;
            double sumC = 0;
            double sumD = 0;
            double sumE = 0;

            sumA = Math.Round(CalculateSumV3(marksA), 3, MidpointRounding.AwayFromZero);
            sumB = Math.Round(CalculateSumV3(marksB), 3, MidpointRounding.AwayFromZero);
            sumC = Math.Round(CalculateSumV3(marksC), 3, MidpointRounding.AwayFromZero);
            sumD = Math.Round(CalculateSumV3(marksD), 3, MidpointRounding.AwayFromZero);

            _result_A = sumA;
            _result_B = sumB;
            _result_C = sumC;
            _result_D = sumD;
            _result_E = sumE;
            // We add chairman reductions (if applied) and
            // calculate the sum based on the weight of each component
            double sum = sumA * (_judgmentAreas.Count > 0 ? _judgmentAreas[0].Weight : 1d) +
                         sumB * (_judgmentAreas.Count > 1 ? _judgmentAreas[1].Weight : 1d) +
                         sumC * (_judgmentAreas.Count > 2 ? _judgmentAreas[2].Weight : 1d) +
                         sumD * (_judgmentAreas.Count > 3 ? _judgmentAreas[3].Weight : 1d) +
                         sumE * (_judgmentAreas.Count > 4 ? _judgmentAreas[4].Weight : 1d) +
                         ChairmanReduction;

            if (sum != Total)
            {
                Total = sum;
                RaiseEvent("Total");
                RaiseEvent("Result_A");
                RaiseEvent("Result_B");
                RaiseEvent("Result_C");
                RaiseEvent("Result_D");
            }
        }
    }
}
