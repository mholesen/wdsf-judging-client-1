﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using Judging_Server.Model;

namespace Judging_Server.Helper
{


    public class CreateJudgesDrawing
    {
        

        public Dictionary<string, List<JudgesDrawing>> Create(List<JudgingArea> judgingAreas, List<Judge> judges, Dictionary<string, List<Heat>> heats, int JudgesPerArea, int NumberOfAreas, int AreaOffset = 0)
        {


            if(NumberOfAreas * JudgesPerArea > judges.Count)
                throw new Exception("Not enough judges for panel size / number of Areas");

            var res = new Dictionary<string, List<JudgesDrawing>>();

            foreach (var dance in heats.Keys)
            {
                var list = new List<JudgesDrawing>();
                res.Add(dance, list);
                foreach (var heat in heats[dance])
                {
                    var random = Random.GetRandomList<Judge>(judges);
                    
                    for (int i = 1; i <= NumberOfAreas; i++)
                    {
                        var drawing = new JudgesDrawing() { Area = i + AreaOffset, Heat = heat };
                        list.Add(drawing);
                        drawing.Judges = new List<Judge>();

                        if (judges.Any(j => j.ComponentToJudge == judgingAreas[i - 1].Component))
                        {
                            drawing.Judges.AddRange(judges.Where(j => j.ComponentToJudge == judgingAreas[i - 1].Component));
                        }
                        else
                        {
                            drawing.Judges.AddRange(random.Take(JudgesPerArea));
                            random.RemoveRange(0, JudgesPerArea);
                        }
                        
                    }
                }
            }
            return res;
        }

        public Dictionary<string, List<JudgesDrawing>> CreateFormation(List<JudgingArea> judgingAreas, List<Judge> judges, Dictionary<string, List<Heat>> heats, int JudgesPerArea)
        {
            var randomJudges = Helper.Random.GetRandomList<Judge>(judges);
             
            var judges1 = randomJudges.Take(6).ToList();

            var judges2 = new List<Judge>();
            for (int i = 6; i < 12; i++)
            {
                judges2.Add(randomJudges[i]);
            }

            var areas1 = new List<JudgingArea>();
            var areas2 = new List<JudgingArea>();

            areas1.Add(judgingAreas.First(a => a.Component == "TQ"));
            areas1.Add(judgingAreas.First(a => a.Component == "MM"));
            areas2.Add(judgingAreas.First(a => a.Component == "TS"));
            areas2.Add(judgingAreas.First(a => a.Component == "CP"));

            var res1 = Create(areas1, judges1, heats, JudgesPerArea, 2, 0);
            var res2 = Create(areas2, judges2, heats, JudgesPerArea, 2, 2);

            foreach (var key in res1.Keys)
            {
                var list1 = res1[key];
                var list2 = res2[key];
                list1.AddRange(list2);
            }

            return res1;
        }
    }
}
