﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;

namespace Judging_Server.Helper
{
    public class Random
    {
        public static int GetRandomNumbers(int count, byte min, byte max)
        {
            // Zufallszahlen erzeugen
            var csp = new RNGCryptoServiceProvider();
            var numbers = new Byte[count];
            csp.GetBytes(numbers);

            // Die Zahlen umrechnen
            double divisor = 256F / (max - min + 1);
            if (min > 0 || max < 255)
            {
                for (int i = 0; i < count; i++)
                {
                    numbers[i] = (byte)((numbers[i] / divisor) + min);
                }
            }

            return (int)numbers[0];
        }

        public static int GetRandom(int max)
        {
            if (max < 256)
            {
                return GetRandomNumbers(1, 0, (byte)max);
            }
            else
            {
                var rnd = new System.Random();
                return rnd.Next(max);
            }
        }

        public List<T> Shuffle<T>(IList<T> originalList)
        {
            var list = originalList.ToList();

            RNGCryptoServiceProvider provider = new RNGCryptoServiceProvider();
            int n = list.Count;
            while (n > 1)
            {
                byte[] box = new byte[1];
                do provider.GetBytes(box);
                while (!(box[0] < n * (Byte.MaxValue / n)));
                int k = (box[0] % n);
                n--;
                T value = list[k];
                list[k] = list[n];
                list[n] = value;
            }

            return list;
        }

        public static List<T> GetRandomList<T>(List<T> list)
        {
            var myList = list.ToList();
            var result = new List<T>();

            while (myList.Count > 1)
            {
                var index = GetRandom(myList.Count - 1);
                result.Add(myList[index]);
                myList.RemoveAt(index);
            }

            if (myList.Count == 1)
            {
                result.Add(myList[0]);
            }

            return result;
        }
    }
}
