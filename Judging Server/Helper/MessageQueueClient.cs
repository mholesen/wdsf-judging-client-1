﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Judging_Server.Helper
{
    using System.Net;
    using System.Net.Sockets;
    using System.Threading;

    using NLog;

    using ZMQ;
    using ZMQ.ZMQExt;

    internal class ServerMessageQueueClient
    {
        private readonly int serverPort;

        private readonly int remotePort;

        private readonly int serviceDiscoveryPort;

        private Thread serviceDiscoveryThread;

        private Thread queueThread;

        private readonly Context context;

        private readonly Action<string> receivedMessageCallback;

        private readonly Logger logger = LogManager.GetCurrentClassLogger();

        public ServerMessageQueueClient(
            int serverPort,
            int remotePort,
            int serviceDiscoveryPort,
            Action<string> callbackAction)
        {
            this.serverPort = serverPort;
            this.remotePort = remotePort;
            this.serviceDiscoveryPort = serviceDiscoveryPort;
            this.receivedMessageCallback = callbackAction;

            context = new Context();

            this.serviceDiscoveryThread = new Thread(this.ServiceDiscovery) { IsBackground = true };
            this.queueThread = new Thread(this.MessagaQueueClient) { IsBackground = true };
            serviceDiscoveryThread.Start();
            queueThread.Start();
        }

        private void ServiceDiscovery()
        {
            var serviceDiscoveryClient = new UdpClient(serviceDiscoveryPort);
            var senderEndPoint = new IPEndPoint(IPAddress.Any, 0);
            while (true)
            {
                try
                {
                    var data = serviceDiscoveryClient.Receive(ref senderEndPoint);
                    var serviceRequest = Encoding.ASCII.GetString(data);
                    if (serviceRequest == "JS 2.0 Server")
                    {
                        // Send back a hello world string to indicate we are here and who we are:
                        var client = new UdpClient();
                        senderEndPoint.Port = serviceDiscoveryPort + 1; // client is listening on the next port
                        var str = string.Format("Hello JS 2.0 Server, Protocol Version 2");
                        data = Encoding.ASCII.GetBytes(str);
                        client.Send(data, data.Length, senderEndPoint);
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
            }
        }

        /// <summary>
        /// Sends a message to a remote client
        /// </summary>
        /// <param name="message">A message e.g. new data</param>
        /// <param name="targetIPAddress">the target IP address of the client</param>
        /// <returns></returns>
        // ReSharper disable once InconsistentNaming
        public string SendMessage(string message, string targetIPAddress)
        {
            if (targetIPAddress == null)
            {
                throw new ArgumentNullException("targetIPAddress");
            }

            using (var socket = context.Socket(ZMQ.SocketType.REQ))
            {
                socket.Connect(string.Format("tcp://{0}:{1}", targetIPAddress, remotePort));
                socket.Send(message, Encoding.Unicode);
                var reply = socket.Recv(Encoding.Unicode, 5000);
                return reply;
            }
        }

        private void MessagaQueueClient()
        {
            while (true)
            {
                using (var socket = context.Socket(ZMQ.SocketType.REP))
                {
                    socket.Bind(string.Format("tcp://*:{0}", serverPort));

                    while (true)
                    {
                        try
                        {
                            var message = socket.Recv(Encoding.Unicode);

                            receivedMessageCallback(message);

                            socket.Send("OK", Encoding.Unicode);
                        }
                        catch (Exception ex)
                        {
                            logger.Error(ex);
                        }
                    }
                }

            }
        }
    }
}
