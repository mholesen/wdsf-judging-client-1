﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using GalaSoft.MvvmLight;

using Judging_Server.Model;

namespace Judging_Server.ViewModel
{
    public class EditRoundConfigurationViewModel : ViewModelBase
    {
        public List<Tuple<string, CalculationVersionEnum, string>> Versions { get; set; }

        public List<string> Sections { get; set; } 

        public double MinimumValue { get; set; }

        public double MaximumValue { get; set; }

        public double Interval { get; set; }

        public EditRoundConfigurationViewModel()
        {
            this.Versions = new List<Tuple<string, CalculationVersionEnum, string>>()
                                {
                                    new Tuple<string, CalculationVersionEnum, string>("V1 (Only Solo)", CalculationVersionEnum.Version1, "V1"),
                                    new Tuple<string, CalculationVersionEnum, string>("V2 (Group, Version 2013)", CalculationVersionEnum.Version2, "V2"),
                                    new Tuple<string, CalculationVersionEnum, string>("V2.1 (Group Versioin 2014)", CalculationVersionEnum.Version2014, "V2014")
                                };

            this.Sections = new List<string>()
                                {
                                    "Standard",
                                    "Latin",
                                    "Formation Latin",
                                    "Formation Standard",
                                    "Showdance Latin",
                                    "Showdance Standard"
                                };

            this.MinimumValue = 5.0;
            this.MaximumValue = 10.0;
            this.Interval = 0.5;

        }
    }
}
