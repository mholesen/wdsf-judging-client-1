﻿using System;
using System.Diagnostics;
using System.IO;
using System.Printing;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Threading;
using System.Windows.Xps.Packaging;

using GalaSoft.MvvmLight.Command;

using Scrutinus.Reports.BasicPrinting;

namespace Scrutinus.Reports
{
    /// <summary>
    /// Interaction logic for PrintReportPage.xaml
    /// </summary>
    public partial class PrintReportPage : Page
    {
        public PrintReportPage()
        {
            this.InitializeComponent();

            this.CloseDocumentViewCommand = new RelayCommand(this.Close);
        }

        public ICommand CloseDocumentViewCommand { get; set; }

        public void Print(AbstractPrinter printer, bool showPrintDlg, bool showPreview, string printerName, int copies, string printTitel, bool landscape = false)
        {
            var document = printer.Document;
            // this.documentViewer.Document = document;
            
            // Find the printing queue
            // print document...
            if (!showPreview)
            {
                PrintQueue queue = null;

                if (showPrintDlg)
                {
                    var dlg = new PrintDialog();
                    var res = dlg.ShowDialog();
                    if (res.HasValue && res.Value)
                    {
                        queue = dlg.PrintQueue;
                    }
                }
                else
                {
                    queue = LocalPrintServer.GetDefaultPrintQueue();
                }

                if (queue != null)
                {
                    var writer = PrintQueue.CreateXpsDocumentWriter(queue);
                    
                    var ticket = new PrintTicket();
                    
                    ticket.CopyCount = copies;
                    if (landscape)
                    {
                        ticket.PageOrientation = PageOrientation.Landscape;    
                    }
                    
                    printer.PrintDocument(false, queue, landscape ? PageOrientation.Landscape : PageOrientation.Portrait, copies, printTitel);
                    // writer.Write(document.DocumentPaginator, ticket);
                }
            }
        }

        private void Close()
        {
            // Go back to print dialog
        }
    }
}