﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using Judging_Server.Model;

using Scrutinus.Reports.BasicPrinting.Controls;

namespace Judging_Server.Print.Controls
{
    /// <summary>
    /// Interaction logic for PrintResultByComponents.xaml
    /// </summary>
    public partial class PrintResultByComponents : UserControl
    {
        private IEnumerable<JudgingArea> judgingAreas;

        private IEnumerable<Judgements> judgements;

        private IEnumerable<Dance> dances;

        private Couple couple;

        public PrintResultByComponents(Couple couple, IEnumerable<JudgingArea> judgingAreas, IEnumerable<Judgements> judgements, IEnumerable<Dance> dances)
        {
            InitializeComponent();
            
            this.judgingAreas = judgingAreas;
            this.judgements = judgements;
            this.dances = dances;

            this.couple = couple;

            this.CreateGrid();
        }

        private void CreateGrid()
        {
            this.TheGrid.ColumnDefinitions.Add(new ColumnDefinition() { Width = new GridLength(2, GridUnitType.Star) });
            this.TheGrid.ColumnDefinitions.Add(new ColumnDefinition() { Width = new GridLength(40, GridUnitType.Pixel) });
            // Create Columns for components
            for (int i = 0; i < this.judgingAreas.Count();i++)
            {
                this.TheGrid.ColumnDefinitions.Add(new ColumnDefinition(){Width = new GridLength(1, GridUnitType.Star)});
            }

            // Create Rows for dances + 1 for header.
            for (int i = 0; i <= dances.Count(); i++)
            {
                this.TheGrid.RowDefinitions.Add(new RowDefinition() { Height = new GridLength(1, GridUnitType.Star)});
            }

            // Create Component Names and Header
            AddText(this.couple.Id.ToString(), 0, 0);
            // Add Components:
            for (var i = 0; i < this.judgingAreas.Count(); i++)
            {
                AddText(this.judgingAreas.ToList()[i].Component, 0, i + 2);
            }

            var row = 1;
            foreach (var dance in dances)
            {
                this.AddDanceResult(dance, row);
                row++;
            }
        }

        private void AddDanceResult(Dance dance, int row)
        {
            this.AddText(dance.LongName, row, 0);

            var column = 2;

            foreach (var judgingArea in judgingAreas)
            {
                AddAreaResult(dance, judgingArea, row, column);
                column++;
            }
        }

        private void AddAreaResult(Dance dance, JudgingArea judgingArea, int row, int column)
        {
            var judgementsForComponent = this.judgements.Where(j => j.Dance == dance.ShortName && j.Couple == couple.Id && j.Area == judgingArea.Component).OrderBy(j => j.Judge);

            var grid = new Grid();

            // We need one column per judge:
            foreach (var judgementse in judgementsForComponent)
            {
                grid.ColumnDefinitions.Add(new ColumnDefinition() { Width = new GridLength(1, GridUnitType.Star)});
            }

            grid.RowDefinitions.Add(new RowDefinition() { Height = new GridLength(1, GridUnitType.Star) });
            grid.RowDefinitions.Add(new RowDefinition() { Height = new GridLength(1, GridUnitType.Star) });

            var col = 0;
            foreach (var judgement in judgementsForComponent)
            {
                grid.SetCellUiElement(new TextBlock() { Text = judgement.Judge}, col, 0);
                grid.SetCellUiElement(new TextBlock() { Text = judgement.Mark != 0d ? judgement.Mark.ToString() : "-"}, col, 1);
                col++;
            }

            var border = new Border()
                             {
                                 BorderBrush = Brushes.Black,
                                 BorderThickness = new Thickness(1),
                                 Margin = new Thickness(4),
                                 Child = grid
                             };

            this.TheGrid.SetCellUiElement(border, column, row);
        }

        private void AddText(string text, int row, int column)
        {
            var textBlock = new TextBlock() { Text = text };
            TheGrid.SetCellUiElement(textBlock, column, row);
        }
    }
}
