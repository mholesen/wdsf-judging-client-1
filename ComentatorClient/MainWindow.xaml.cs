﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using CommonLibrary;

namespace ComentatorClient
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private List<ResultData> DanceResult;
        private List<ResultData> TotalResult;
        private FileSystemWatcher watcher;

        public MainWindow()
        {
            InitializeComponent();

            //ReadResultDance(@"C:\Temp\eJudge\TV2\DanceTotal.txt");
            //ReadResultTotal(@"C:\Temp\eJudge\TV2\TotalResult.txt");
            //ReadCoupleResult(@"C:\Temp\eJudge\TV2\DanceResult.txt");

            watcher = new FileSystemWatcher();
            watcher.Path = ComentatorSettings.Default.InputPath;
            watcher.Changed += watcher_Changed;
            watcher.Created += watcher_Changed;
            watcher.EnableRaisingEvents = true;

        }

        void watcher_Changed(object sender, FileSystemEventArgs e)
        {
            if (!e.Name.EndsWith(".txt"))
                return;

            if (e.Name == "DanceTotal.txt")
            {
                Dispatcher.Invoke(new Action(()=> ReadResultDance(e.FullPath)));
            }

            if (e.Name == "TotalResult.txt")
            {
                Dispatcher.Invoke(new Action(() => ReadResultTotal(e.FullPath)));
            }
            
            if (e.Name == "DanceResult.txt")
            {
                Dispatcher.Invoke(new Action(() => 
                ReadCoupleResult(e.FullPath)));
                
            }
        }

        private StreamReader OpenStream(string filename)
        {
            int count = 0;
            StreamReader reader = null;
            while (reader == null && count < 20)
            {
                try
                {
                    reader = new StreamReader(filename);
                }
                catch (Exception ex)
                {
                    count++;
                    System.Threading.Thread.Sleep(300);
                }
            }

            return reader;
        }

        public void ReadResultDance(string filename)
        {
            StreamReader reader = null;
            try
            {
                reader = OpenStream(filename);
                if (reader == null) return;
                var data = reader.ReadLine().Split(';');
                LabelDance.Content = data[1];
                DanceResult = new List<ResultData>();
                while (!reader.EndOfStream)
                {
                    data = reader.ReadLine().Split(';');
                    DanceResult.Add(new ResultData()
                        {
                            Name = data[3],
                            Country = data[4],
                            Number = data[2],
                            Place = Int32.Parse(data[0]),
                            Points = data[1].ParseString()
                        });
                }

                LabelStatus.Visibility = Visibility.Visible;
                reader.Close();
            }
            catch (Exception ex)
            {
                
            }
            
            if(reader != null) reader.Close();
            ResultDance.ItemsSource = DanceResult;
        }

        public void ReadResultTotal(string filename)
        {
            StreamReader reader = null;
            try
            {
                reader = OpenStream(filename);
                if (reader == null) return;

                TotalResult = new List<ResultData>();
                while (!reader.EndOfStream)
                {
                    var data = reader.ReadLine().Split(';');
                    TotalResult.Add(new ResultData()
                    {
                        Name = data[3],
                        Country = data[4],
                        Number = data[2],
                        Place = Int32.Parse(data[0]),
                        Points = data[1].ParseString()
                    });
                }

                LabelStatus.Visibility = Visibility.Visible;
            }
            catch (Exception ex)
            {

            }
            if(reader != null) reader.Close();
            ResultTotal.ItemsSource = TotalResult;
        }

        public void ReadCoupleResult(string filename)
        {
            StreamReader reader = null;
            try
            {
                reader = OpenStream(filename);
                if (reader == null) return;

                var data = reader.ReadLine().Split(';');
                DanceName.Content = data[1];
                data = reader.ReadLine().Split(';');
                CoupleName.Content = String.Format("{0} ({1})", data[1], data[2]);
                data = reader.ReadLine().Split(';');
                Component1.Content = "TQ: " + data[0];
                Component2.Content = "MM: " + data[1];
                Component3.Content = "PS: " + data[2];
                Component4.Content = "CP: " + data[3];

                data = reader.ReadLine().Split(';');
                CouplePlace.Content = "Place in Dance: " + data[1];
                CoupleTotal.Content = "Score: " + data[0];
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            if(reader != null) reader.Close();
        }
    }

    

}
