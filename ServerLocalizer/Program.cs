﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ServerLocalizer
{
    class Program
    {
        static void Main(string[] args)
        {
            var localizer = new ServiceLocalizer();

            string result = null;
            int count = 0;

            while (result == null && count < 10)
            {
                Console.WriteLine("Searching eJudge Server ...");
                result = localizer.SendBroadcast(9123, "eJudge!");
                count++;
            }

            if(result != null)
            {
                Console.WriteLine("Found Server at " + result);
                Process.Start("cmd.exe", "/C net use j: /delete");
                Process.Start("cmd.exe", string.Format(@"/C net use j: \\{0}\ejudge /user:wdsf wdsf", result));
                Console.WriteLine("Please check driver j: is available");
            }
            else
            {
                Console.WriteLine("Could not find server");
            }

            Console.WriteLine("Press enter to exit");
            Console.ReadLine();
        }
    }
}
