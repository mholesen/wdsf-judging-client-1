﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Forms;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Monitor_Preview
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();

            var thread = new Thread(this.PreviewThread);

            if (Screen.AllScreens.Count() == 1)
            {
                System.Windows.MessageBox.Show("Only one screen found");
                return;
            }

            thread.IsBackground = true;
            thread.Start();
        }

        void PreviewThread()
        {
            var screenIndex = 1;

            while (true)
            {
                var bitmap = new Bitmap(Screen.AllScreens[screenIndex].Bounds.Width, Screen.AllScreens[screenIndex].Bounds.Height);

                //var bitmap = new Bitmap(Screen.PrimaryScreen.Bounds.Width, Screen.PrimaryScreen.Bounds.Height);

                var graphics = Graphics.FromImage(bitmap);

                graphics.CopyFromScreen(Screen.AllScreens[screenIndex].Bounds.Left, Screen.AllScreens[screenIndex].Bounds.Top, 0, 0, bitmap.Size);

                this.Dispatcher.Invoke(() => this.PreviewImage.Source = this.ConvertBitmapToBitmapImage(bitmap));

                Thread.Sleep(500);
            }
        }

        private BitmapImage ConvertBitmapToBitmapImage(Bitmap bitmap)
        {
            var memoryStream = new MemoryStream();
            bitmap.Save(memoryStream, ImageFormat.Png);
            var bitmapImage = new BitmapImage();
            bitmapImage.BeginInit();
            bitmapImage.StreamSource = new MemoryStream(memoryStream.ToArray());
            bitmapImage.EndInit();

            return bitmapImage;
        }
    }
}
