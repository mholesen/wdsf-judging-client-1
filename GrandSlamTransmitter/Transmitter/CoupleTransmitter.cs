﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using Wdsf.Api.Client.Models;

using Competition = GrandSlamTransmitter.Model.Competition;

namespace GrandSlamTransmitter.Transmitter
{
    public class CoupleTransmitter : ResultTransmitAbstract
    {
        public CoupleTransmitter(string user, string password)
            : base(user, password)
        {
            
        }

        protected override void UpdateResults(Model.Competition TPScompetition, CompetitionDetail competition, bool clearBeforeSending)
        {
            // now load the registered couples from the database
            var couplesRegistered = apiClient.GetCoupleParticipants(competition.Id);
            // Für jetzt erst mal alle löschen:
            if (clearBeforeSending)
            {
                foreach (var c in couplesRegistered)
                {
                    apiClient.DeleteCoupleParticipant(c.Id);
                }
                couplesRegistered.Clear();
            }

            if (couplesRegistered.Count == 0)
            {
                this.RegisterCouples(TPScompetition, competition);
                couplesRegistered = apiClient.GetCoupleParticipants(competition.Id);
            }

            var registeredCouples = new List<ParticipantCoupleDetail>();
            foreach (var cp in couplesRegistered)
            {
                if (TPScompetition.Couples.Any(t => t.Number.ToString() == cp.StartNumber))
                {
                     Console.WriteLine("Loading Details for for {0}", cp.Couple);
                    var paricipant = apiClient.GetCoupleParticipant(cp.Id);
                    registeredCouples.Add(paricipant);
                }
            }
        

            foreach (var couple in TPScompetition.Couples)
            {
                Logger.Instance.Log(Logger.Level.Info, "Processing " + couple.Number);
                Console.WriteLine("Sending Results for {0}", couple.Couple.NiceName);
                var wdsf_couple = registeredCouples.SingleOrDefault(r => r.StartNumber == couple.Number);
                if (wdsf_couple == null)
                {
                    Logger.Instance.Log(Logger.Level.Error, "Couple " + couple.Number + " not registered at WDSF database!");
                    continue;
                }
                wdsf_couple.Rank = couple.PlaceFrom.ToString();
                wdsf_couple.Points = couple.TotalMarking.ToString();

                this.UpdateMarks(wdsf_couple, couple, TPScompetition);

                var isSuccess = apiClient.UpdateCoupleParticipant(wdsf_couple);
                if (!isSuccess)
                {
                    Logger.Instance.Log(Logger.Level.Error, apiClient.LastApiMessage);
                }
            }
        }

        private CoupleDetail CreateNewCouple(Model.Competitor couple, int competitionId)
        {
            Logger.Instance.Log(Logger.Level.Warning,
                                    "Couple with Man " + couple.Couple.MINMan + " and Woman " + couple.Couple.MINWoman +
                                    ": both person knwon at WDSF Database but not a valid couple. Couple will be created if possible");

            var newCouple = new Wdsf.Api.Client.Models.CoupleDetail()
            {
                Country = "Estonia",
                Name = "Vitali Kozmin - Valeria Milova",
                Division = "Professional",
                ManMin = 10099297,
                WomanMin = 10098398,
                ManMinSpecified = true,
                WomanMinSpecified = true,
                Status = "Active",
                AgeGroup = "Adult"
            };
            try
            {
                // Save the couple but catch exceptions (because of some violations in Country Names
                var ret = apiClient.SaveCouple(newCouple);
                newCouple = apiClient.Get<CoupleDetail>(ret);
                Logger.Instance.Log(Logger.Level.Info, String.Format("Couple {0} created.", newCouple.Name));

                var participant = new ParticipantCoupleDetail()
                {
                    CompetitionId = competitionId,
                    CoupleId = newCouple.Id,
                    StartNumber = couple.Number
                };

                try
                {
                    apiClient.SaveCoupleParticipant(participant);
                }
                catch (Exception ex)
                {
                    Logger.Instance.Log(ex);
                }
                return newCouple;
            }
            catch (Exception ex)
            {
                Logger.Instance.Log(Logger.Level.Error, String.Format("Could not create couple {0} - {1}: {2}, last api message: {3}", couple.Couple.LastNameHe, couple.Couple.LastNameShe, ex.Message, apiClient.LastApiMessage));
            }

            return null;
        }

        protected void UpdateMarks(ParticipantCoupleDetail couple, Model.Competitor competitor, Model.Competition competition)
        {

            if (competitor.Results == null)
            {
                return; // keine Daten
            }

            var round = couple.Rounds.SingleOrDefault(r => r.Name == this._round);
            if (round == null)
            {
                round = new Round() { Name = this._round };
                couple.Rounds.Add(round);
            }
            else
            {
                round.Dances.Clear();
            }

            var results = competitor.Results["F"]; // As a tradition, all results of a round are stored as 'F'

            foreach (var dance in competition.Dances)
            {
                var danceresult = new Dance() { Name = this.translateDanceName[dance] };

                round.Dances.Add(danceresult);

                var list = results.Where(m => m.Dance == dance);

                foreach (var mark in list)
                {
                    var offical = this._officals.SingleOrDefault(o => o.AdjudicatorChar == mark.Judge.WDSFSign);
                    Score score;

                    if (offical == null)
                    {
                        MessageBox.Show("Official with WDSF ID " + mark.Judge.WDSFSign + " not found");
                        return;
                    }

                    score = new OnScale2Score
                    {
                        OfficialId = offical.Id,
                        TQ = (decimal)mark.MarkA,
                        MM = (decimal)mark.MarkB,
                        PS = (decimal)mark.MarkC,
                        CP = (decimal)mark.MarkD,
                        // Reduction currently not used
                        Reduction = 0
                    };

                    danceresult.Scores.Add(score);
                }
            }
        }

        private void RegisterCouples(Competition competition, CompetitionDetail wdsfCompetition)
        {
            foreach (var couple in competition.Couples)
            {
                if (couple.Couple.WDSFID == "")
                {
                    var c = new CoupleDetail()
                    {
                        AgeGroup = "Adult",
                        Country = couple.Couple.Country,
                        ManMinSpecified = false,
                        WomanMinSpecified = false,
                        UnknownManName = couple.Couple.FirstNameHe + " " + couple.Couple.LastNameHe,
                        UnknownManNameSpecified = true,
                        UnknownWomanName = couple.Couple.FirstNameShe + " " + couple.Couple.LastNameShe,
                        UnknownWomanNameSpecified = true,
                        Division = "General",
                        Status = "Active"
                    };

                    var uri = apiClient.SaveCouple(c);
                    var c2 = apiClient.Get<CoupleDetail>(uri);
                    couple.Couple.WDSFID = c2.Id;
                }

                if (!couple.Couple.WDSFID.StartsWith("rls"))
                    couple.Couple.WDSFID = "wdsf-" + couple.Couple.WDSFID;

                var participant = new ParticipantCoupleDetail()
                {
                    CompetitionId = wdsfCompetition.Id,
                    CoupleId = couple.Couple.WDSFID,
                    StartNumber = couple.Number
                };

                try
                {
                    apiClient.SaveCoupleParticipant(participant);
                }
                catch (Exception ex)
                {
                    Logger.Instance.Log(ex);
                }
            }
        }
    }
}
