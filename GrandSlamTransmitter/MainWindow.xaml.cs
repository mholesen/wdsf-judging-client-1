﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.IO;
using System.Xml.Linq;

using CommonLibrary;

using GrandSlamTransmitter.Model;
using GrandSlamTransmitter.Transmitter;

namespace GrandSlamTransmitter
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        [DllImport("shell32.dll", EntryPoint = "ShellExecute")]
        public static extern long ShellExecute(int hwnd, string cmd, string file, string param1, string param2, int swmode);

        public MainWindow()
        {
            InitializeComponent();
            txtDirectory.Text = ApplicationSettings.Default.DataPath;

            AppDomain.CurrentDomain.UnhandledException += CurrentDomainOnUnhandledException;
        }

        private void CurrentDomainOnUnhandledException(object sender, UnhandledExceptionEventArgs unhandledExceptionEventArgs)
        {
            var exception = unhandledExceptionEventArgs.ExceptionObject as Exception;
            if (exception != null)
            {
                MessageBox.Show(string.Format("{0}\r\n{1}", exception.Message, exception.StackTrace));
                if (exception.InnerException != null)
                {
                    MessageBox.Show(exception.InnerException.Message);
                }
            }
            else
            {
                MessageBox.Show("Unhandled Exception of unknown type");
            }
        }

        private double CalculateSumV3(List<double> marks, double componentWeight)
        {
            if (marks.Count != 3)
            {
                return 0;
            }

            var distance = marks[1] - marks[0];
            var lowerWeight = 1 / (1 + distance * distance);
            distance = marks[2] - marks[1];
            var upperWeight = 1 / (1 + distance * distance);
            var sum = marks[0] * lowerWeight + marks[1] + marks[2] * upperWeight;
            sum /= 1 + 1 * lowerWeight + 1 * upperWeight;

            return sum * componentWeight;
        }

        private void btnStart_Click(object sender, RoutedEventArgs e)
        {
            // Wir erzeugen hier das Model und dann laden wir die Wertungen hoch, ganz einfach
            var competition = new Model.Competition();
            // open file

            if (File.Exists(String.Format("{0}\\data.txt", this.txtDirectory.Text)))
            {
                LoadDataFromCsv(competition);
            }
            else
            {
                LoadDataFromXml(competition);
            }
            
            if(!competition.Judges.All(j=> !string.IsNullOrEmpty(j.Offical.WDSFSign)))
            {
                MessageBox.Show("Now all judges have a WDSF Sign assigned.", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            // Wir laden die Wertungen
            foreach (var offical in competition.Judges)
            {
                var fileName = string.Format("{0}\\Result_{1}.txt", this.txtDirectory.Text, offical.Offical.Sign);

                if (File.Exists(fileName))
                {
                    this.LoadMarkings(offical, competition, fileName);
                }
                // for 10 dance, we now try to load the other dances as well:
                fileName = string.Format("{0}\\ResultS_{1}.txt", this.txtDirectory.Text, offical.Offical.Sign);

                if (File.Exists(fileName))
                {
                    this.LoadMarkings(offical, competition, fileName);
                }
                fileName = string.Format("{0}\\ResultL_{1}.txt", this.txtDirectory.Text, offical.Offical.Sign);

                if (File.Exists(fileName))
                {
                    this.LoadMarkings(offical, competition, fileName);
                }

                fileName = string.Format("{0}\\Result_{1}_S.txt", this.txtDirectory.Text, offical.Offical.Sign);

                if (File.Exists(fileName))
                {
                    this.LoadMarkings(offical, competition, fileName);
                }
            }
            // Wir berechnen schnell mal die Totals, damit wir Plätze setzen können und dann auch 
            // Diese Plätze übermitteln können.
            foreach (var couple in competition.Couples)
            {
                couple.TotalMarking = 0;
                foreach (var dance in competition.Dances)
                {
                    if (ApplicationSettings.Default.Version != 2)
                    {
                        var markings = couple.Results["F"].Where(m => m.Dance == dance);
                        var a = markings.OrderBy(m => m.MarkA).ToList();
                        var sumtotal = markings.Sum(m => m.MarkA);
                        var sum = (markings.Sum(m => m.MarkA) - a[0].MarkA - a[a.Count - 1].MarkA) / (a.Count - 2) * competition.WeightA;
                        a = markings.OrderBy(m => m.MarkB).ToList();
                        sum += (markings.Sum(m => m.MarkB) - a[0].MarkB - a[a.Count - 1].MarkB) / (a.Count - 2) * competition.WeightB;
                        a = markings.OrderBy(m => m.MarkC).ToList();
                        sum += (markings.Sum(m => m.MarkC) - a[0].MarkC - a[a.Count - 1].MarkC) / (a.Count - 2) * competition.WeightC;
                        a = markings.OrderBy(m => m.MarkD).ToList();
                        sum += (markings.Sum(m => m.MarkD) - a[0].MarkD - a[a.Count - 1].MarkD) / (a.Count - 2) * competition.WeightD;
                        
                        //chairman reduction is missing
                        couple.TotalMarking += Math.Round(sum, 3, MidpointRounding.AwayFromZero);
                    }
                    else
                    {
                        var markings = couple.Results["F"].Where(m => m.Dance == dance).ToList();
                        // Nun berechnen wir jeweils den Mittelwert aus den einzelnen Gebieten
                        var a = markings.Where(m => m.MarkA > 0).OrderBy(m => m.MarkA).Select(m => m.MarkA).ToList();
                        if (a.Count != 3) DebugMarks(markings);
                        var sum = Math.Round(CalculateSumV3(a, competition.WeightA), 3, MidpointRounding.AwayFromZero);
                        a = markings.Where(m => m.MarkB > 0).OrderBy(m => m.MarkB).Select(m => m.MarkB).ToList();
                        if (a.Count != 3)
                            DebugMarks(markings);
                        sum += Math.Round(CalculateSumV3(a, competition.WeightB), 3, MidpointRounding.AwayFromZero);
                        a = markings.Where(m => m.MarkC > 0).OrderBy(m => m.MarkC).Select(m => m.MarkC).ToList();
                        if (a.Count != 3)
                            DebugMarks(markings);
                        sum += Math.Round(CalculateSumV3(a, competition.WeightC), 3, MidpointRounding.AwayFromZero);
                        a = markings.Where(m => m.MarkD > 0).OrderBy(m => m.MarkD).Select(m => m.MarkD).ToList();
                        if (a.Count != 3)
                            DebugMarks(markings);
                        sum += Math.Round(CalculateSumV3(a, competition.WeightD), 3, MidpointRounding.AwayFromZero);

                        couple.TotalMarking += Math.Round(sum, 3, MidpointRounding.AwayFromZero);
                    }
                }
            }
            // Nun berechnen wir die Plätze. Wir sortieren einfach nach der Summe und numerieren durch
            var result = competition.Couples.OrderByDescending(c => c.TotalMarking).ToList();

            this.SetPlaces(result);

            int excludeRank = 0;
            if (int.TryParse(textBoxExcludeRanks.Text, out excludeRank) && excludeRank > 0)
            {
                for (int i = 0; i < excludeRank; i++)
                {
                    result[i].PlaceFrom = 0;
                    result[i].PlaceTo = 0;
                }
            }

            // wir legen los:
            competition.TransmitToWDSF = true;
            var transmitter = TransmitterFactory.GetTransmitter(competition, ApplicationSettings.Default.WdsfUser, ApplicationSettings.Default.WdsfPassword);
            
            transmitter.SendResult(competition, RoundName.Text, txtMINChairman.Text, this.ChkClearData.IsChecked.HasValue && this.ChkClearData.IsChecked.Value);

            this.SetState();
        }

        void SetPlaces(List<Competitor> list)
        {
            int placeFrom = 1;
            int index = 0;

            while (index < list.Count)
            {
                // Find the same placees:
                var sameIndex = index;
                while (sameIndex < list.Count && list[index].TotalMarking == list[sameIndex].TotalMarking)
                {
                    sameIndex++;
                }

                for (int i = index; i < sameIndex; i++)
                {
                    list[i].PlaceFrom = placeFrom;
                    list[i].PlaceTo = placeFrom + (sameIndex - index - 1);
                }

                placeFrom = sameIndex + 1;

                index = sameIndex;
            }
        }

        private void DebugMarks(List<Mark> markings)
        {
            foreach (var marking in markings)
            {
                Debug.WriteLine("{0} : {1} | {2} {3} {4} {5}", marking.Judge.Sign, "", marking.MarkA, marking.MarkB, marking.MarkC, marking.MarkD);
            }
        }

        

        private void LoadMarkings(OfficalWithRole offical, Competition competition, string fileName)
        {
            System.Globalization.CultureInfo ci = new System.Globalization.CultureInfo("de-DE");

            var stream = new StreamReader(fileName);
            while (!stream.EndOfStream)
            {
                var data = Model.Model.TPSSplitter(stream.ReadLine());
                var couple = competition.Couples.SingleOrDefault(c => c.Number == Int32.Parse(data[2]));

                if (couple == null)
                {
                    MessageBox.Show("Could not find couple #" + data[2] + " from results in start list");
                    continue;
                }

                Mark mark = null;

                if (ApplicationSettings.Default.Version != 2)
                {
                    mark = new Model.Mark()
                               {
                                   Judge = offical.Offical,
                                   Dance = data[1],
                                   MarkA = data[3].ParseString(),
                                   MarkB = data[4].ParseString(),
                                   MarkC = data[5].ParseString(),
                                   MarkD = data[6].ParseString(),
                                   MarkE = data[7].ParseString(),
                               };
                    if (mark.MarkA > 10)
                    {
                        System.Diagnostics.Debugger.Break();
                    }
                }
                else
                {
                    mark = new Mark
                               {
                                   Judge = offical.Offical,
                                   Dance = data[1],
                                   MarkA = 0,
                                   MarkB = 0,
                                   MarkC = 0,
                                   MarkD = 0,
                                   MarkE = 0
                               };

                    var area = Int32.Parse(data[3]);

                    var value = data[4].ParseString();
                    switch (area)
                    {
                        case 1:
                            mark.MarkA = value;
                            break;
                        case 2:
                            mark.MarkB = value;
                            break;
                        case 3:
                            mark.MarkC = value;
                            break;
                        case 4:
                            mark.MarkD = value;
                            break;
                    }
                }
                if (!couple.Results.ContainsKey("F"))
                {
                    couple.Results.Add("F", new List<Mark>());
                }

                couple.Results["F"].Add(mark);
            }
            stream.Close();
        }

        private void LoadDataFromXml(Competition competition)
        {
            var doc = XDocument.Load(String.Format("{0}\\JSConfiguration.xml", txtDirectory.Text));

            var jsVersion = doc.Element("JSConfiguration").Element("JSVersion");
            competition.IsFormation = jsVersion.Attribute("IsFormation") != null && jsVersion.Attribute("IsFormation").Value.ToLower() == "true";

            var components = doc.Descendants("Component");
            foreach (var component in components)
            {
                var weight = component.Attribute("Weight").Value.ParseString();

                if (component.Attribute("ShortName").Value == "TQ")
                {
                    competition.WeightA = weight;
                }

                if (component.Attribute("ShortName").Value == "MM")
                {
                    competition.WeightB = weight;
                }

                if (component.Attribute("ShortName").Value == "PS" || component.Attribute("ShortName").Value == "TS")
                {
                    competition.WeightC = weight;
                }

                if (component.Attribute("ShortName").Value == "CP")
                {
                    competition.WeightD = weight;
                }
            }

            doc = XDocument.Load(String.Format("{0}\\data.xml", txtDirectory.Text));

            // Read Event Data
            var root = doc.Element("Data");
            var eventData = root.Element("Event");
            if (eventData == null) throw new Exception("No event data in XML data file");

            competition.WDSF_Id = eventData.Attribute("WdsfId").Value;

            competition.Dances = new List<string>();
            competition.Judges = new List<Model.OfficalWithRole>();
            competition.Couples = new List<Competitor>();

            foreach (var danceXml in doc.Descendants("Dance"))
            {
                competition.Dances.Add(danceXml.Attribute("ShortName").Value);
            }

            // Now read the the judges:
            foreach (var judgeXml in doc.Descendants("Judge"))
            {
                var min = 0;
                Int32.TryParse(judgeXml.Attribute("MIN").Value, out min);
                var judge = new Offical()
                {
                    Club = judgeXml.Attribute("Country").Value,
                    LastName = judgeXml.Attribute("Name").Value,
                    Sign = judgeXml.Attribute("Sign").Value,
                    WDSFSign = judgeXml.Attribute("WdsfSign").Value,
                    MIN = min
                };
                competition.Judges.Add(new OfficalWithRole() { Offical = judge, Role = "Adjudicator" });
            }
            // Read the couples
            foreach (var coupleXml in doc.Descendants("Couple"))
            {
                var names = coupleXml.Attribute("Name").Value.Split('/');
                var namesHe = names[0].Split('_');

                string[] namesShe = new string[0];

                if (names.Length > 1)
                {
                    namesShe = names[1].Split('_');
                }

                var country = coupleXml.Attribute("Country").Value;
                var number = coupleXml.Attribute("Number").Value;
                var min = coupleXml.Attribute("MIN").Value;

                var couple = new Model.Couple()
                {
                    Country = country,
                    FirstNameHe = namesHe[0],
                    FirstNameShe = names.Length > 1 ? namesShe[0] : "",
                    LastNameHe = namesHe.Length > 1 ? namesHe[1] : "",
                    LastNameShe = namesShe.Length > 1 ? namesShe[1] : "",
                    Id = Int32.Parse(number),
                    WDSFID = min
                    // MinID = data[3]
                };

                var competitor = new Model.Competitor()
                {
                    Competition = competition,
                    Couple = couple,
                    LastRound = "F",
                    Number = couple.Id
                };
                competitor.Results = new Dictionary<string, List<Model.Mark>>();
                //competitor.Results.Add("F", new List<TPSModel.Mark());
                competition.Couples.Add(competitor);
            }

        }

        private void LoadDataFromCsv(Competition competition)
        {
            var stream = new StreamReader(String.Format("{0}\\data.txt", txtDirectory.Text));
            stream.ReadLine();
            stream.ReadLine();
            var sCount = stream.ReadLine();

            var weight = Model.Model.TPSSplitter(stream.ReadLine())[1].ParseString();
            competition.WeightA = weight;
            weight = Model.Model.TPSSplitter(stream.ReadLine())[1].ParseString();
            competition.WeightB = weight;
            weight = Model.Model.TPSSplitter(stream.ReadLine())[1].ParseString();
            competition.WeightC = weight;
            weight = Model.Model.TPSSplitter(stream.ReadLine())[1].ParseString();
            competition.WeightD = weight;

            var data = Model.Model.TPSSplitter(stream.ReadLine());
            competition.WDSF_Id = data[0];

            competition.Dances = new List<string>();
            competition.Judges = new List<OfficalWithRole>();
            competition.Couples = new List<Competitor>();

            data = Model.Model.TPSSplitter(stream.ReadLine());

            for (int i = 0; i < data.Length; i += 2)
            {
                if (!string.IsNullOrWhiteSpace(data[i]))
                {
                    competition.Dances.Add(data[i]);
                }
            }
            // Read Judges until line = ";"
            var line = stream.ReadLine();
            do
            {
                int min = 0;

                data = Model.Model.TPSSplitter(line);
                int.TryParse(data[5], out min);
                var judge = new Model.Offical()
                {
                    Club = data[4],
                    LastName = data[3],
                    Sign = data[1],
                    WDSFSign = data[2],
                    MIN = min
                };

                competition.Judges.Add(new OfficalWithRole() { Offical = judge, Role = "Adjudicator" });
                line = stream.ReadLine();
            } while (line != ";");
            // Read Couples until ;
            line = stream.ReadLine();
            do
            {
                data = Model.Model.TPSSplitter(line);

                var couple = new Model.Couple()
                {
                    Country = data[2],
                    FirstNameHe = data[1],
                    FirstNameShe = "",
                    LastNameHe = "",
                    LastNameShe = "",
                    Id = Int32.Parse(data[0]),
                    WDSFID = data[3]
                    // MinID = data[3]
                };
                if (data.Length > 3) couple.MinID = data[3];

                var competitor = new Model.Competitor()
                {
                    Competition = competition,
                    Couple = couple,
                    LastRound = "F",
                    Number = couple.Id
                };
                competitor.Results = new Dictionary<string, List<Model.Mark>>();
                //competitor.Results.Add("F", new List<TPSModel.Mark());
                competition.Couples.Add(competitor);
                line = stream.ReadLine();
            } while (line != ";");
            // Rest interessiert nicht .... 
            stream.Close();
        }

        private void SetState()
        {
            // Wir erzeugen hier das Model und dann laden wir die Wertungen hoch, ganz einfach
            var competition = new Model.Competition();
            // open file

            if (File.Exists(String.Format("{0}\\data.txt", this.txtDirectory.Text)))
            {
                LoadDataFromCsv(competition);
            }
            else
            {
                LoadDataFromXml(competition);
            }

            var transmitter = TransmitterFactory.GetTransmitter(competition, "ogroehn", "Nuwanda01");

            if (chkInProgress.IsChecked.HasValue && chkInProgress.IsChecked.Value)
            {
                transmitter.SetStateInProgress(competition);
            }

            if (chkProcessing.IsChecked.HasValue && chkProcessing.IsChecked.Value)
                transmitter.SetStateProcessing(competition);

            if (chkClosed.IsChecked.HasValue && chkClosed.IsChecked.Value)
                transmitter.SetStateClosed(competition);

            ShellExecute(0, "open", "notepad", Logger.Instance.File, "", 5);
        }

        private void BtnSetState_OnClick(object sender, RoutedEventArgs e)
        {
            this.SetState();
        }
    }
}
